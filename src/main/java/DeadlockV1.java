

/**
 * Bei dieser Version haben wir den Deadlock so aufgel�st, indem ein Lock-Object erstellt wurde. 
 * Damit k�nnen die beiden Freunde/Threads (Alphonse und Gaston) sich gegenseitig verbeugen.
 * Egal welcher der beiden Freunde /Threads zuerst startet, sie stossen nicht mit den K�pfen
 * zusammen und k�nnen sich unbeschadet verbeugen.
 * Jedoch m�ssten allf�llig weitere Freunde/ Threads warten, bis Gaston und Alphonse mit dem verbeugen und zur�ckverbeugen
 * fertig sind.
 * 
 * 
 * @author Stephanie Kaswurm und Anna-Lena Klaus
 * 
 */
public class DeadlockV1 {

	void doStuff() throws InterruptedException {
		final Friend alphonse = new Friend("Alphonse");
		final Friend gaston = new Friend("Gaston");
		Thread gastonThread = new Thread(new Runnable() {
			public void run() {
				alphonse.bow(gaston);
			}
		}, "Gaston");

		Thread alphonseThread = new Thread(new Runnable() {
			public void run() {
				gaston.bow(alphonse);
			}
		}, "Alphonse");

		gastonThread.start();
		alphonseThread.start();

		alphonseThread.join();
		gastonThread.join();

	}
	// 2 neue Threads werden geboren.
	public static void main(String[] args) throws InterruptedException {
		DeadlockV1 d = new DeadlockV1();
		d.doStuff();
	}
}

class Friend {
	
	// Lock-Object wird erstellt.
	private static final Object LOCK = new Object();
	
	private final String name;

	public Friend(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void bow(Friend bower) {
		
		// Synchronisation mit Lock-Object
		synchronized (LOCK) {			
			System.out.format("%s: %s" + "  has bowed to me!%n", this.name,
					bower.getName());
		
			bower.bowBack(this);
		}
	}

	public void bowBack(Friend bower) {
		
		// Synchronisation mit Lock-Object
		synchronized (LOCK) {			
			System.out.format("%s: %s" + " has bowed back to me!%n", this.name,
					bower.getName());
		}
	}
}
