

/**
 * Bei dieser Version wird der Deadlock so aufgel�st, dass die beiden Freunde synchronized werden.
 * So lange die Threads nicht gleichzeitig starten ist das Deadlock-Problem gel�st
 * (bzw. sie stossen ihre K�pfe nicht aneinandner).
 * 
 * @author Stephanie Kaswurm und Anna-Lena Klaus
 */
public class DeadlockV2 {

	void doStuff() throws InterruptedException {
		final Friend alphonse = new Friend("Alphonse");
		final Friend gaston = new Friend("Gaston");
		Thread gastonThread = new Thread(new Runnable() {
			public void run() {
				alphonse.bow(gaston);
			}
		}, "Gaston");

		Thread alphonseThread = new Thread(new Runnable() {
			public void run() {
				gaston.bow(alphonse);
			}
		}, "Alphonse");

		//Threads werden gestartet.
		gastonThread.start();
		alphonseThread.start();

		// Der eine Thread wartet auf den anderen, bis dieser seine Ausf�hrung abgeschlossen hat.
		alphonseThread.join();
		gastonThread.join();

	}

	public static void main(String[] args) throws InterruptedException {
		DeadlockV2 d = new DeadlockV2();
		d.doStuff();
	}
}

class Friend2 {
	
	private final String name;

	public Friend2(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void bow(Friend2 bower) {
		
		//Synchronisation der Threads
		synchronized (this) {			
			System.out.format("%s: %s" + "  has bowed to me!%n", this.name,
					bower.getName());
		
			bower.bowBack(this);
		}
	}

	public void bowBack(Friend2 bower) {
		
		//Synchronisation der Threads
		synchronized (this) {			
			System.out.format("%s: %s" + " has bowed back to me!%n", this.name,
					bower.getName());
		}
	}
}
